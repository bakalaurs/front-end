export class CommentVM {
    id: number;
    text: string;
    userId: number;
}