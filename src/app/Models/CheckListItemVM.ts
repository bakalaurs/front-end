export class CheckListItemVM {
    id: number;
    name: string;
    checked: boolean;
}