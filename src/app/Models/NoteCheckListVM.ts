import { CheckListItemVM } from "./CheckListItemVM";

export class NoteCheckListVM {
    id: number;
    name: string;
    CheckListItem: CheckListItemVM[];
}