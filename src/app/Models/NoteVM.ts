import { LabelVM } from "./LabelVM";
import { NoteCheckListVM } from "./NoteCheckListVM";
import { CommentVM } from "./CommentVM";

export class NoteVM {
    id: number;
    name: string;
    description: string;
    labels: LabelVM[];
    noteCheckLists: NoteCheckListVM[];
    comments: CommentVM[];
}