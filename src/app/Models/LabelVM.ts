export class LabelVM {
    id: number;
    name: string;
    color: string;
}