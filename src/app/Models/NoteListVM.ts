import { NoteVM } from "./NoteVM";

export class NoteListVM {
    id: number;
    name: string;
    notes: NoteVM[];
}