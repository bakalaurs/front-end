import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BoardsComponent } from './boards/boards.component';
import { NoteListsComponent } from './note-lists/note-lists.component'
import { NotesComponent } from './notes/notes.component';

const routes: Routes = [
  { path: '', redirectTo: '/', pathMatch: 'full' },
  { path: 'boards', component: BoardsComponent} ,
  { path: 'noteLists/:id', component: NoteListsComponent} ,
  { path: 'note/:id', component: NotesComponent}
 
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
