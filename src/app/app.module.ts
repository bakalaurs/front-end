import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from "@angular/flex-layout";
import { MatButtonModule, MatCheckboxModule,  MatInputModule, MatMenuModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { BoardsComponent } from './boards/boards.component';
import { NoteListsComponent } from './note-lists/note-lists.component';
import { BoardService } from './Services/board.service';
import { NoteListService } from './Services/note-list.service';
import { NotesComponent } from './notes/notes.component';
import { NoteService } from './Services/note.service';



@NgModule({
  declarations: [
    AppComponent,
    BoardsComponent,
    NoteListsComponent,
    NotesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule, 
    HttpClientModule, 
    AppRoutingModule,
    FlexLayoutModule,
    MatCheckboxModule,
    MatInputModule,
    MatButtonModule,
    MatMenuModule,
    BrowserAnimationsModule
  ],
  providers: [
    BoardService,
    NoteListService,
    NoteService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
