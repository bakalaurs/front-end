import { Component, OnInit } from '@angular/core';

import { BoardVM } from '../Models/BoardVM';
import { BoardService } from '../Services/board.service';
import { NoteListsComponent } from '../note-lists/note-lists.component';

@Component({
  selector: 'app-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.css']
})
export class BoardsComponent implements OnInit {  
  title = 'Eznote';
  boards: BoardVM[];
  board: BoardVM;
  adminId:number = 1;  
  editId: number = 0;

  constructor(private boardService: BoardService) { }

  ngOnInit() {
    this.getBoards();
  }

  getBoards(): void{
    this.boardService.getBoards(this.adminId).subscribe(boards => this.boards = boards);
  }  

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.board = new BoardVM;
    this.board.AdminId = this.adminId;
    this.board.name = name;

    this.boardService.addBoard(this.board).subscribe(board => {
      this.boards.push(board);
    });
  }

  delete(board: BoardVM): void {
      this.boards = this.boards.filter(b => b !== board);
      this.boardService.deleteBoard(board.id).subscribe();    
  }

  renameBoard(id: number){
    this.boardService.updateBoard(this.boards.find(x => x.id == id)).subscribe();
  }

  editActive(n: number){
    this.editId = n;
  }

}
