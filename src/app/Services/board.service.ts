import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { BoardVM } from '../Models/BoardVM';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class BoardService {

  private boardUrl = 'http://localhost:50814/api/board';

  constructor(private http: HttpClient) { }

  getBoards(id: number): Observable<BoardVM[]> {
    const url = this.boardUrl+'/'+id;
    return this.http.get<BoardVM[]>(url);
  }

  addBoard (board: BoardVM): Observable<BoardVM> {
    return this.http.post<BoardVM>(this.boardUrl, board);
  }

  deleteBoard (board: BoardVM | number): Observable<BoardVM> {
    const id = typeof board === 'number' ? board : board.id;
    const url = `${this.boardUrl}/${id}`;

    return this.http.delete<BoardVM>(url, httpOptions);
  }

  updateBoard (board: BoardVM): Observable<BoardVM>{
    return this.http.put<BoardVM>(this.boardUrl, board);
  }

}
