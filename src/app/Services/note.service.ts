import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { NoteVM } from '../Models/NoteVM';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class NoteService {

  private noteUrl = 'http://localhost:50814/api/note';

  constructor(private http: HttpClient) { }

  getNote(id: number): Observable<NoteVM> {
    return this.http.get<NoteVM>(this.noteUrl+"/"+id);
  }

  addNote (note: NoteVM, id: number): Observable<NoteVM> {
    return this.http.post<NoteVM>(this.noteUrl+"/"+id,note);
  }

}
