import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { NoteListVM } from '../Models/NoteListVM';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class NoteListService {

  private noteListUrl = 'http://localhost:50814/api/notelist';

  constructor(private http: HttpClient) { }

  // board id for all notelists from that board
  getNoteLists(id: number): Observable<NoteListVM[]> {
    const url = this.noteListUrl+'/'+id;
    return this.http.get<NoteListVM[]>(url);
  }

  addNoteList (noteList: NoteListVM, id: number): Observable<NoteListVM> {
    return this.http.post<NoteListVM>(this.noteListUrl+"/"+id, noteList);
  }

  deleteNoteList (noteList: NoteListVM | number): Observable<NoteListVM> {
    const id = typeof noteList === 'number' ? noteList : noteList.id;
    const url = `${this.noteListUrl}/${id}`;
    return this.http.delete<NoteListVM>(url, httpOptions);
  }

  updateNoteList (noteList: NoteListVM, id: number): Observable<NoteListVM>{
    return this.http.put<NoteListVM>(this.noteListUrl+"/"+id, noteList);
  }

}
