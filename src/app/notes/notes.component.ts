import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatButtonModule, MatCheckboxModule,  MatInputModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {NoteVM} from '../Models/NoteVM';
import {NoteService} from '../Services/note.service';

import {LabelVM} from '../Models/LabelVM';
import {NoteCheckListVM} from '../Models/NoteCheckListVM';
import {CheckListItemVM} from '../Models/CheckListItemVM';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit {

  note: NoteVM;
  label: LabelVM;
  isChecked: boolean;
  checkList: NoteCheckListVM;
  listItem: CheckListItemVM;
 
  constructor(private noteService: NoteService,
    private route: ActivatedRoute) {
      route.params.subscribe(val => {
        this.getNote();
      });
     }

  ngOnInit() {    
    this.getNote();
    this.label = new LabelVM();
    this.label.color = "#ff0000";
    this.checkList = new NoteCheckListVM();
    this.listItem = new CheckListItemVM();

  }

  getNote(): void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.noteService.getNote(id).subscribe(note => {console.log(note); this.note = note});     
  } 

  onCheck(checkListId:number ,checkItemId: number): void{
    this.isChecked = this.note.noteCheckLists.find(list => list.id == checkListId).CheckListItem.find(item => item.id == checkItemId).checked;
    this.note.noteCheckLists.find(list => list.id == checkListId).CheckListItem.find(item => item.id == checkItemId).checked = !this.isChecked;
  }

  addLabel(){
    if(this.note.labels == null){
      this.note.labels = new Array<LabelVM>();
    }

    if(this.label.name){
    this.note.labels.push(this.label);

    this.label = new LabelVM();
    this.label.color = "#ff0000";
    }
  }

  addCheckList(){
    if(this.note.noteCheckLists == null)
    {
      this.note.noteCheckLists = new Array<NoteCheckListVM>();
    }

    if(this.checkList.name)
    {
      this.note.noteCheckLists.push(this.checkList);
      this.checkList = new NoteCheckListVM();
    }
  }

  addCheckListItem(id: number){
    if(!this.note.noteCheckLists.find(x =>x.id == id).CheckListItem)
      {
        this.note.noteCheckLists.find(x =>x.id == id).CheckListItem = new Array<CheckListItemVM>();
      }

    if(this.listItem.name)
    {      
      this.note.noteCheckLists.find(x =>x.id == id).CheckListItem.push(this.listItem);
      this.listItem = new CheckListItemVM();
      document.getElementById("itemFromList").innerHTML = "";
    }
    
  }
}
