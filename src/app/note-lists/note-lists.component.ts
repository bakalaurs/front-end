import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NoteListVM } from '../Models/NoteListVM';
import { NoteListService } from '../Services/note-list.service';
import { BoardVM } from '../Models/BoardVM';
import { NoteService } from '../Services/note.service';
import { NoteVM } from '../Models/NoteVM';

@Component({
  selector: 'app-note-lists',
  templateUrl: './note-lists.component.html',
  styleUrls: ['./note-lists.component.css']
})
export class NoteListsComponent implements OnInit {

  noteLists: NoteListVM[];
  noteList: NoteListVM;
  note: NoteVM;
  id: number;
  editId: number = 0;

  constructor(private noteListService: NoteListService,
    private noteService: NoteService,
    private route: ActivatedRoute) {
      route.params.subscribe(val => {
        this.getNoteLists();
      });
     }

  ngOnInit() {
    this.getNoteLists();
  }

  getNoteLists(): void{
    this.id = +this.route.snapshot.paramMap.get('id');
    this.noteListService.getNoteLists(this.id).subscribe(noteLists => this.noteLists = noteLists);
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    const id = +this.route.snapshot.paramMap.get('id');
    this.noteList = new NoteListVM();
    this.noteList.name = name;
    this.noteList.notes = new Array<NoteVM>();
    this.noteListService.addNoteList(this.noteList,id).subscribe(noteList => {this.noteLists.push(noteList);});
  }

  delete(noteList: NoteListVM): void {
    this.noteLists = this.noteLists.filter(n => n !== noteList);
    this.noteListService.deleteNoteList(noteList.id).subscribe();    
  }

  addNote(name: string, ID: number): void {
    name = name.trim();
    if (!name) { return; }
    this.note = new NoteVM();
    this.note.name = name;
    this.noteService.addNote(this.note,ID).subscribe(note => {this.noteLists.find(x => x.id == ID).notes.push(note);});
  }

  renameNoteList(id: number){
    this.noteListService.updateNoteList(this.noteLists.find(x => x.id == id),id).subscribe();
  }

  editNoteList(n: number){
    this.editId = n;
  }

}
